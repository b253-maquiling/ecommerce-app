import { useAuthContext } from "./useAuthContext";
import Swal from "sweetalert2";

export const useAddToCart = () => {
  const { user } = useAuthContext();

  const addCart = async (id, quantity) => {
    const response = await fetch("http://localhost:5001/user/checkout", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${user.token}`,
      },
      body: JSON.stringify({ productId: id, quantity }),
    });
    const json = await response.json();

    if (!response.ok) {
      Swal.fire({
        title: "Authentication Failed",
        icon: "error",
        text: "Check your login credentials and try again.",
      });
    }

    if (response.ok) {
      Swal.fire({
        title: "Product Added to cart",
        icon: "success",
        text: "Check your cart to checkout",
      });
    }
  };
  return addCart;
};
