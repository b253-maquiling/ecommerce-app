import { useState } from "react";
import { useProductContext } from "../hooks/useProductContext";
import { useAuthContext } from "../hooks/useAuthContext";
import Swal from "sweetalert2";

const ProductForm = () => {
  const { dispatch } = useProductContext();
  const { user } = useAuthContext();

  const [productName, setProductName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const product = { productName, description, price };
    const response = await fetch("http://localhost:5001/products/create", {
      method: "POST",
      body: JSON.stringify(product),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    });
    const json = await response.json();

    if (response.ok) {
      Swal.fire({
        title: `${json.productName} is Added`,
        icon: "success",
        text: "A new product is added",
      });
      dispatch({ type: "CREATE_PRODUCTS", payload: json });
      setProductName("");
      setDescription("");
      setPrice("");
    }
  };

  return (
    <form className="create" onSubmit={handleSubmit}>
      <h3>Add a New Product</h3>

      <label>Product Name:</label>
      <input
        type="text"
        onChange={(e) => setProductName(e.target.value)}
        value={productName}
      />

      <label>Price:</label>
      <input
        type="number"
        inputMode="numeric"
        onChange={(e) => setPrice(e.target.value)}
        value={price}
      />

      <label>Description:</label>
      <textarea
        cols="30"
        rows="10"
        onChange={(e) => setDescription(e.target.value)}
        value={description}
      ></textarea>

      <button>Add Product</button>
    </form>
  );
};

export default ProductForm;
