import { useEffect } from "react";
import { Container, Row } from "react-bootstrap";
import ActiveProductsDetails from "../components/ActiveProductsDetails";
import { useAuthContext } from "../hooks/useAuthContext";
import { useProductContext } from "../hooks/useProductContext";

const Products = () => {
  const { products, dispatch } = useProductContext();
  const { user } = useAuthContext();

  useEffect(() => {
    const fetchActiveProducts = async () => {
      const response = await fetch("http://localhost:5001/products/active");

      const json = await response.json();
      if (response.ok) {
        dispatch({ type: "SET_PRODUCTS", payload: json });
      }
    };

    fetchActiveProducts();
  }, [dispatch]);

  console.log(products);
  return (
    <>
      <div className="banner">
        <h1>All Products</h1>
      </div>

      <Container className="product">
        <Row>
          {products &&
            products.map((product) => (
              <ActiveProductsDetails key={product._id} product={product} />
            ))}
        </Row>
      </Container>
    </>
  );
};

export default Products;
