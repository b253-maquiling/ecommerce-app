import { useProductContext } from "./useProductContext";
import { useAuthContext } from "./useAuthContext";
import Swal from "sweetalert2";

export const useUpdateProduct = () => {
  const { dispatch } = useProductContext();
  const { user } = useAuthContext();

  const updateProduct = async (productDetails) => {
    const { productId, productName, description, price } = productDetails;
    const response = await fetch(
      `http://localhost:5001/products/update/${productId}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${user.token}`,
        },
        body: JSON.stringify({
          productName,
          description,
          price,
        }),
      }
    );
    const json = await response.json();

    if (!response.ok) {
      Swal.fire({
        title: "Authentication Failed",
        icon: "error",
        text: "Something went wrong.",
      });
    }

    if (response.ok) {
      Swal.fire({
        title: "Update Successful",
        icon: "success",
        text: "Product is now updated",
      });

      dispatch({ type: "UPDATE_PRODUCTS", payload: json });
    }
  };
  return updateProduct;
};
