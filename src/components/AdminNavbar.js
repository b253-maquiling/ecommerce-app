import {
  Container,
  Navbar,
  Nav,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

import { useAuthContext } from "../hooks/useAuthContext";
import { useLogout } from "../hooks/useLogout";

const AdminNavbar = () => {
  const { user } = useAuthContext();
  const logout = useLogout();

  return (
    <>
      <nav className="main-menu">
        <Navbar.Brand as={Link} to="/" className="nav-brand-sm" sticky="top">
          <h3>Pogi</h3>
          <hr />
        </Navbar.Brand>
        <div className="sidebar-links">
          <ul>
            <li>
              <NavLink to="/">
                <span className="material-symbols-outlined">
                  space_dashboard
                </span>
                <span className="nav-text">Dashboard</span>
              </NavLink>
            </li>
            <li className="has-subnav">
              <NavLink to="/">
                <span className="material-symbols-outlined">group</span>
                <span className="nav-text">User Orders</span>
              </NavLink>
            </li>
            <li className="has-subnav">
              <NavLink to="/">
                <span className="material-symbols-outlined">
                  manage_accounts
                </span>
                <span className="nav-text">Set Admin</span>
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
      <Navbar
        expand="lg"
        className="px-5 py-3 align-items-center border-bottom"
      >
        <Container fluid>
          <Nav className="ms-auto">
            <div className="d-flex align-items-center">
              <Navbar.Text className="material-symbols-outlined">
                notifications
              </Navbar.Text>
              <DropdownButton
                title={
                  <span className="material-symbols-outlined">
                    account_circlearrow_drop_down
                  </span>
                }
                hidden={user ? false : true}
              >
                <Dropdown.Item>{user && <>{user.email}</>}</Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item onClick={logout}>Logout</Dropdown.Item>
              </DropdownButton>
            </div>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
};

export default AdminNavbar;
