
import { useAuthContext } from "../hooks/useAuthContext";

import AdminNavbar from "./AdminNavbar";
import UserNavbar from "./UserNavbar";

const AppNavbar = () => {
  const { user } = useAuthContext();

  return user && user.isAdmin ? <AdminNavbar /> : <UserNavbar />;
};

export default AppNavbar;
