import { NavLink, useParams } from "react-router-dom";

import { useState, useEffect } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import { useAuthContext } from "../hooks/useAuthContext";

import { useAddToCart } from "../hooks/useAddToCart";

import productImg6 from "../images/product-img-test6.jpg";

const ProductInfo = () => {
  const { id } = useParams();
  const { user } = useAuthContext();
  const addCart = useAddToCart();

  const [productName, setProductName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(0);

  const addToCart = async () => {
    await addCart(id, quantity);
  };

  useEffect(() => {
    const fetchProduct = async () => {
      const response = await fetch(`http://localhost:5001/products/${id}`);

      const json = await response.json();
      if (response.ok) {
        setProductName(json.productName);
        setDescription(json.description);
        setPrice(json.price);
      }
    };

    fetchProduct();
  }, [id]);

  return (
    <Container fluid className="productDetails">
      <Container>
        <Row className="d-flex align-items-center">
          <Col>
            <img src={productImg6} alt="product-title" />
          </Col>
          <Col>
            <h1>{productName}</h1>
            <p>{description}</p>
            <p>&#8369; {price}</p>

            <label>Quantity</label>
            <Row className="quantity">
              <Col onClick={() => setQuantity(Math.max(0, quantity - 1))}>
                <span className="material-symbols-outlined">remove</span>
              </Col>
              <Col>{quantity}</Col>
              <Col onClick={() => setQuantity(quantity + 1)}>
                <span className="material-symbols-outlined">add</span>
              </Col>
            </Row>

            {!user ? (
              <NavLink to="/login" className="mx-0">
                <Button className="dark-primary-btn">Sign up to buy</Button>
              </NavLink>
            ) : (
              <Button
                className={quantity <= 0 ? "dark-primary-btn-disable" : "dark-primary-btn"}
                onClick={addToCart}
                disabled={quantity <= 0 ? true : false}
              >
                Add to cart
              </Button>
            )}
          </Col>
        </Row>
      </Container>
    </Container>
  );
};

export default ProductInfo;
