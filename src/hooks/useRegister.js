import { useState } from "react";
import Swal from "sweetalert2";

export const useRegister = () => {
  const [successRegister, setSuccessRegister] = useState(false);

  const register = async (info) => {
    const response = await fetch("http://localhost:5001/user/register", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        firstName: info.firstName,
        lastName: info.lastName,
        email: info.email,
        mobileNo: info.mobileNo,
        password: info.password1,
      }),
    });

    const json = await response.json();

    if (!response.ok) {
      Swal.fire({
        title: "Duplicate email found",
        icon: "error",
        text: "Please provide a different email",
      });
    }

    if (response.ok) {
      setSuccessRegister(json);
      Swal.fire({
        title: "Registration Successful",
        icon: "success",
        text: "Welcome to Zuitt!",
      });
    }
  };

  return { register, successRegister };
};
