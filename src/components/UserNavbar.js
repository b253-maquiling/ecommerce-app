import {
  Container,
  Navbar,
  Nav,
  Button,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

import { useAuthContext } from "../hooks/useAuthContext";
import { useLogout } from "../hooks/useLogout";

const UserNavbar = () => {
  const { user } = useAuthContext();
  const logout = useLogout();

  return (
    <Navbar expand="lg" className="px-5 py-3 align-items-center border-bottom" sticky="top">
      <Container fluid>
        <Navbar.Brand as={Link} to="/" className="nav-brand-sm">
          <h3>Pogi</h3>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="align-items-center">
          <Nav className="ms-auto md-font-body">
            <Nav.Link as={NavLink} to="/">
              Home
            </Nav.Link>

            <Nav.Link as={NavLink} to="/products">
              Products
            </Nav.Link>

            <Nav.Link as={NavLink} to="/login" hidden={!user ? false : true}>
              Login
            </Nav.Link>

            <Nav.Link as={NavLink} to="/register" hidden={!user ? false : true}>
              Register
            </Nav.Link>
          </Nav>
          <Nav className="ms-auto">
            <div className="d-flex align-items-center">
              <Navbar.Text className="material-symbols-outlined" hidden={user ? false : true}>
                shopping_cart
              </Navbar.Text>
              <NavLink to="/login" className="mx-0">
                <Button
                  className="dark-primary-btn"
                  hidden={!user ? false : true}
                >
                  Sign up
                </Button>
              </NavLink>
              <DropdownButton
                title={
                  <span className="material-symbols-outlined">
                    account_circlearrow_drop_down
                  </span>
                }
                hidden={user ? false : true}
              >
                <Dropdown.Item>{user && <>{user.email}</>}</Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item onClick={logout}>Logout</Dropdown.Item>
              </DropdownButton>
            </div>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default UserNavbar;
