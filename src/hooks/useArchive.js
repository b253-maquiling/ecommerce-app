import { useProductContext } from "./useProductContext";
import { useAuthContext } from "./useAuthContext";
import Swal from "sweetalert2";

export const useArchive = () => {
  const { dispatch } = useProductContext();
  const { user } = useAuthContext();

  const archiveProduct = async (archiveValue) => {
    const { isOn, productId } = archiveValue;
    const response = await fetch(
      `http://localhost:5001/products/archive/${productId}`,
      {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${user.token}`,
        },
        body: JSON.stringify({
          "isActive": !isOn,
        }),
      }
    );
    const json = await response.json();
console.log(json);
    if (!response.ok) {
      Swal.fire({
        title: "Authentication Failed",
        icon: "error",
        text: "Something went wrong.",
      });
    }

    if (response.ok) {
        Swal.fire({
          title: "Archive Successful",
          icon: "success",
          text: `Product is now isActive ${!isOn}`,
        });
      }
  };
  return archiveProduct;
};
