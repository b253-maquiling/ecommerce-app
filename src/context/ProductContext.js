import { createContext, useEffect, useReducer } from "react";
import { useAuthContext } from "../hooks/useAuthContext";

export const ProductContext = createContext();

export const productsReducer = (state, action) => {
  switch (action.type) {
    case "SET_PRODUCTS":
      return {
        products: action.payload,
      };
    case "CREATE_PRODUCTS":
      return {
        products: [action.payload, ...state.products],
      };
    case "UPDATE_PRODUCTS":
      const updatedProductIndex = state.products.findIndex(
        (product) => product._ud === action.payload._id
      );

      if (updatedProductIndex === -1) {
        return state;
      }

      const updatedProducts = [...state.product];
      updatedProducts[updatedProductIndex] = action.payload;
      return {
        ...state,
        products: updatedProducts,
      };
    case "ARCHIEVE_PRODUCTS":
      return {
       
      };
    default:
      return state;
  }
};

export const ProductContextProvider = ({ children }) => {
  const { user } = useAuthContext();

  const [state, dispatch] = useReducer(productsReducer, {
    products: null,
  });

  useEffect(() => {
    const fetchProducts = async () => {
      if(user && user.token){
        const response = await fetch("http://localhost:5001/products/", {
          method: "GET",
          headers: {
            Authorization: `Bearer ${user.token}`,
          },
        });
  
        const json = await response.json();
        if (response.ok) {
          dispatch({ type: "SET_PRODUCTS", payload: json });
        }

      }
    };
    fetchProducts();
  }, [state.products]);

  return (
    <ProductContext.Provider value={{ ...state, dispatch }}>
      {children}
    </ProductContext.Provider>
  );
};
