import { useEffect, useState } from "react";
import { useArchive } from "../hooks/useArchive";

const Switch = ({ value, onChange, productId }) => {
  const [isOn, setIsOn] = useState(value);

  const archiveProduct = useArchive();

  const handleClick = () => {
      setIsOn(!isOn);
      const archiveValue = { isOn, productId };
    if (onChange) {
      onChange(!isOn);
      archiveProduct(archiveValue);
    }
  };

  return (
    <div
      style={{
        width: 40,
        height: 20,
        borderRadius: 10,
        backgroundColor: isOn ? "green" : "red",
        display: "flex",
        alignItems: "center",
        justifyContent: isOn ? "flex-end" : "flex-start",
        cursor: "pointer",
      }}
      onClick={handleClick}
    >
      <div
        style={{
          width: 16,
          height: 16,
          borderRadius: "50%",
          backgroundColor: "white",
          boxShadow: "0px 1px 2px rgba(0, 0, 0, 0.3)",
          marginLeft: isOn ? 4 : 0,
          marginRight: isOn ? 0 : 4,
          transition: "margin 0.2s ease-out",
        }}
      />
    </div>
  );
};

export default Switch;
