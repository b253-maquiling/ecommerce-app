import ProductModal from "./ProductModal";

import formatDistanceToNow from "date-fns/formatDistanceToNow";
import Switch from "./Switch";

const updateHandle = async (e) => {
  e.preventDefault();
};

const ProductDetails = ({ product }) => {
  return (
    <div className="workout-details d-flex mb-5">
      <div className="col-4">
        <h4>{product.productName}</h4>
        <p>{product.description}</p>
        <p>&#8369; {product.price}</p>
        <p>
          {!product.updatedOn
            ? `Created ${formatDistanceToNow(new Date(product.createOn), {
                addSuffix: true,
              })}`
            : `Updated ${formatDistanceToNow(new Date(product.updatedOn), {
                addSuffix: true,
              })}`}
        </p>
      </div>
      <div className="col-4 d-flex">
        <span
          className="material-symbols-outlined "
          onClick={updateHandle}
          style={{ marginRight: "20px" }}
        >
          edit
        </span>
        <Switch
          productId={product._id}
          value={product.isActive}
          onChange={(value) => console.log(value)}
        />
      </div>
      <ProductModal product={product} />
    </div>
  );
};

export default ProductDetails;
