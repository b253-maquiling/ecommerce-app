import { BrowserRouter, Routes, Route } from "react-router-dom";
import AppNavbar from "./components/AppNavbar";
import { useAuthContext } from "./hooks/useAuthContext";
import {
  Home,
  Login,
  Register,
  Dashboard,
  Products,
  Error,
  ProductInfo,
} from "./pages";

function App() {
  const { user } = useAuthContext();
  return (
    <BrowserRouter>
      <AppNavbar />
      <Routes>
        <Route
          path="/"
          element={user && user.isAdmin ? <Dashboard /> : <Home />}
        />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/products" element={<Products />} />
        <Route path="/products/:id" element={<ProductInfo />} />
        <Route path="*" element={<Error />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
