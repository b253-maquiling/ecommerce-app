import { useState, useEffect } from "react";
import { Form, Button, Container } from "react-bootstrap";

import { useUpdateProduct } from "../hooks/useUpdateProduct";

const ProductModal = ({ product }) => {
  const [productName, setProductName] = useState(product.productName);
  const [description, setDescription] = useState(product.description);
  const [price, setPrice] = useState(product.price);
  const [isActive, setIsActive] = useState(false);

  const updateProduct = useUpdateProduct();

  const productId = product._id;
  useEffect(() => {
    if (
      productName !== product.productName ||
      description !== product.description ||
      price !== product.price
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [productName, description, price]);

  const updateHandle = async (e) => {
    e.preventDefault();

    setIsActive(false)
    const productDetails = { productId, productName, description, price };

    await updateProduct(productDetails);
  };
  return (
    <Container>
      <Form onSubmit={updateHandle}>
        <h2>Update Product</h2>
        <Form.Group className="mb-3">
          <Form.Label>Product Name:</Form.Label>
          <Form.Control
            type="text"
            placeholder={productName}
            value={productName}
            onChange={(e) => setProductName(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Price:</Form.Label>
          <Form.Control
            type="text"
            placeholder={price}
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Description:</Form.Label>
          <Form.Control
            type="text"
            placeholder={description}
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            required
          />
        </Form.Group>

        <Button
          variant={isActive ? "success " : "danger"}
          type="submit"
          id="submitBtn"
          disabled={isActive ? false : true}
        >
          Update
        </Button>
      </Form>
    </Container>
    // <div className="workout-details">
    //   <h4>{product.productName}</h4>
    //   <p>{product.description}</p>
    //   <p>&#8369; {product.price}</p>
    //   <p>
    //     Created{" "}
    //     {formatDistanceToNow(new Date(product.createOn), { addSuffix: true })}
    //   </p>
    // </div>
  );
};

export default ProductModal;
